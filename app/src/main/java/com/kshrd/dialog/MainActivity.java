package com.kshrd.dialog;

import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindArray;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private static String TAG;

    @BindArray(R.array.brands)
    String[] brands;

    ArrayList<String> mUserItems = new ArrayList<>();
    boolean[] checked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        TAG = getClass().getSimpleName();
        checked = new boolean[brands.length];

        //brands = getResources().getStringArray(R.array.brands);
    }

    @OnClick(R.id.btnSimpleDialog)
    void onSimpleDialogClicked() {
        showSimpleDialog();
    }

    @OnClick(R.id.btnSingleListDialog)
    void onSingleListDialogClicked() {
        showSingleListDialog();
    }

    @OnClick(R.id.btnRadioButtonDialog)
    void onRadioButtonDialogClicked() {
        showRadioButtonDialog();
    }

    @OnClick(R.id.btnMultipleChoiceDialog)
    void onMultipleChoiceClicked() {
        showMultipleChoiceDialog();
    }

    @OnClick(R.id.btnCustomDialog)
    void onCustomDialogClicked(){
        showCustomDialog();
    }

    private void showCustomDialog() {
        FragmentManager fm = getSupportFragmentManager();
        DialogFragment customDialog = new CustomDialogFragment();
        customDialog.show(fm, "Dialog Fragment");
    }

    private void showMultipleChoiceDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle(R.string.brand)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {

                        String temp = "";

                        for (int i = 0; i < mUserItems.size(); i++) {
                            temp += mUserItems.get(i);
                            if (i != (mUserItems.size() - 1)){
                                temp += ",";
                            }
                        }
                        Log.e(TAG, temp);
                    }
                })
                .setMultiChoiceItems(brands, checked, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int position, boolean isChecked) {

                        if (isChecked) {
                            mUserItems.add(brands[position]);
                        } else if (mUserItems.contains(brands[position])) {
                            mUserItems.remove(brands[position]);
                        }
                    }
                })
                .show();
    }

    private void showRadioButtonDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle(R.string.brand)
                .setPositiveButton(R.string.ok, null)
                .setSingleChoiceItems(brands, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int position) {
                        Toast.makeText(MainActivity.this, brands[position], Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    private void showSingleListDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle("Brand")
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setItems(brands, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int position) {
                        Toast.makeText(MainActivity.this, brands[position], Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    void showSimpleDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setCancelable(false)
                .setTitle("Confirmation")
                .setMessage("Are you sure?")
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .show();

//        AlertDialog alertDialog = builder.show();
//        alertDialog.show();
    }
}
