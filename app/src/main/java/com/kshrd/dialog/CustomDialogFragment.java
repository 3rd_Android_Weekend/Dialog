package com.kshrd.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by pirang on 6/24/17.
 */

public class CustomDialogFragment extends DialogFragment {

    @BindView(R.id.etEmail)
    EditText etEmail;

    @BindView(R.id.etPwd)
    EditText etPwd;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.custom_dialog, container);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @OnClick(R.id.btnLogin)
    void onLoginClicked(){
        String email = etEmail.getText().toString();
        String pwd = etPwd.getText().toString();

        Toast.makeText(getActivity(), email + " , " + pwd, Toast.LENGTH_SHORT).show();
        dismiss();
    }
}
